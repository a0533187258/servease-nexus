import { verifyAuthToken } from './Providers/jwt';
import { Request, Response, NextFunction } from 'express';

export const authentication = (req: Request, res: Response, next: NextFunction) => {
  try {
    const tokenFromClient = req.headers.authorization;
    const Id = req.params.id;
    if (!tokenFromClient) {
      return res.status(401).json({
        message: 'No token provided',
      });
    }
    const userInfo = verifyAuthToken(tokenFromClient);
    if (!userInfo || typeof(userInfo) === 'string') {
      return res.status(401).json({
        message: 'Invalid token',
      });
    }
    req.userInfo = userInfo;
    next();
  } catch (err) {
    return res.status(401).json({
      message: 'Invalid request',
    });
  }
};

export const admunAuthentication = (req: Request, res: Response, next: NextFunction) => {
  try {
    const isAdmin = req.userInfo.roles.includes('isAdmin');;
    if (!isAdmin) {
      return res.status(401).json({
        message: 'Inappropriate permissions',
      });
    }
    next();
  } catch (err) {
    return res.status(401).json({
      message: 'Invalid request',
    });
  }
};