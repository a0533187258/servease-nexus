import jwt from 'jsonwebtoken';
import { config } from 'dotenv';
import { Admins } from 'src/admins/entities/admin.entity';

config({ path: '../../../.env' });

let key = process.env.JWT_PRIVATE_KEY || 'ServEase-Nexus';

const generateAuthToken = (admin: Admins) => {
  const { id, email, active, isAdmin } = admin;
  const token = jwt.sign({ id, email, active, isAdmin }, key);
  return token;
};

const verifyAuthToken = (tokenFromClient: string): string | jwt.JwtPayload => {
  try {
    const userData = jwt.verify(tokenFromClient, key);
    return userData;
  } catch (err) {
    console.log(err);
    return null;
  }
};

export { verifyAuthToken, generateAuthToken };
