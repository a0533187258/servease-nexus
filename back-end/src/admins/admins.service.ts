import { Injectable } from '@nestjs/common';
import { CreateAdminInput } from './dto/create-admin.input';
import { UpdateAdminInput } from './dto/update-admin.input';
import { Admins } from './entities/admin.entity';
import { Repository } from 'typeorm';
import { AdminType } from './dto/admin.schema';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class AdminsService {
  constructor(
    @InjectRepository(Admins)
    private readonly adminRepository: Repository<Admins>,
  ) {}

  create(createAdminInput: CreateAdminInput): Promise<AdminType> {
    const newAdmin = this.adminRepository.create(createAdminInput);
    return this.adminRepository.save(newAdmin);
  }

  findAll(): Promise<Admins[]> {
    return this.adminRepository.find();
  }

  findOne(id: string): Promise<Admins | undefined> {
    return this.adminRepository.findOne({ where: { id } });
  }

  async findByUsernameAndPassword(
    email: string,
    password: string,
  ): Promise<Admins | undefined> {
    return this.adminRepository.findOne({ where: { email, password } });
  }

  async update(
    id: string,
    updateAdminInput: UpdateAdminInput,
  ): Promise<Admins> {
    await this.adminRepository.update(id, updateAdminInput);
    return this.adminRepository.findOne({ where: { id } });
  }

  async remove(id: string): Promise<Boolean> {
    const result = await this.adminRepository.delete(id);
    return result.affected > 0;
  }
}
