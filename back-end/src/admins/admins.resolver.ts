import { Resolver, Query, Mutation, Args, Context } from '@nestjs/graphql';
import { AdminsService } from './admins.service';
import { Admins } from './entities/admin.entity';
import { CreateAdminInput } from './dto/create-admin.input';
import { UpdateAdminInput } from './dto/update-admin.input';
import { AdminType } from './dto/admin.schema';
import { admunAuthentication, authentication } from 'src/auth/authService';
import { Request, Response } from 'express';

@Resolver(() => Admins)
export class AdminsResolver {
  constructor(private readonly adminsService: AdminsService) {}

  @Mutation(() => AdminType)
  createAdmin(@Args('createAdminInput') createAdminInput: CreateAdminInput,
  @Context() context: { req: Request; res: Response },
  ) {
    const { req, res } = context;
    authentication(req, res, () => {});
    admunAuthentication(req, res, () => {});
    return this.adminsService.create(createAdminInput);
  }

  @Query(() => [AdminType], { name: 'admins' })
  findAll(
    @Context() context: { req: Request; res: Response },
  ) {
    const { req, res } = context;
    authentication(req, res, () => {});
    admunAuthentication(req, res, () => {});
    return this.adminsService.findAll();
  }

  @Query(() => AdminType, { name: 'admin' })
  findOne(@Args('id') id: string,
  @Context() context: { req: Request; res: Response },
  ) {
    const { req, res } = context;
    authentication(req, res, () => {});
    admunAuthentication(req, res, () => {});
    return this.adminsService.findOne(id);
  }

  @Query(() => AdminType, { name: 'findByUsernameAndPassword' })
  findByUsernameAndPassword(
    @Args('username') username: string,
    @Args('password') password: string,
  ) {
    return this.adminsService.findByUsernameAndPassword(username, password);
  }

  @Mutation(() => AdminType)
  updateAdmin(
    @Args('updateAdminInput') updateAdminInput: UpdateAdminInput,
    @Args({ name: 'id' }) id: string,
    @Context() context: { req: Request; res: Response },
    ) {
      const { req, res } = context;
      authentication(req, res, () => {});
      admunAuthentication(req, res, () => {});
    return this.adminsService.update(id, updateAdminInput);
  }

  @Mutation(() => AdminType)
  removeAdmin(@Args('id') id: string,
  @Context() context: { req: Request; res: Response },
  ) {
    const { req, res } = context;
    authentication(req, res, () => {});
    admunAuthentication(req, res, () => {});
    return this.adminsService.remove(id);
  }
}
