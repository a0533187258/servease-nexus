import {
  Matches,
  MinLength,
  IsString,
  IsNotEmpty,
  IsOptional,
  IsBoolean,
} from '@nestjs/class-validator';
import { emailRegEx, passwordRegEx, phoneRegEx, phonePassRegEx } from './RegEx';
import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class UpdateAdminInput {
  @Matches(emailRegEx, {
    message: 'Invalid email format',
  })
  @Field()
  email?: string;

  @Matches(passwordRegEx, { message: 'Password is not strong enough' })
  @MinLength(8, { message: 'Password must be at least 8 characters long' })
  @Field()
  password?: string;

  @IsString()
  @MinLength(2, { message: 'Name must be at least 2 letters long.' })
  @IsNotEmpty()
  @Field()
  firstName?: string;

  @IsString()
  @MinLength(2, { message: 'Name must be at least 2 letters long.' })
  @IsNotEmpty()
  @Field()
  lastName?: string;

  @IsString()
  @Matches(phoneRegEx, { message: 'Invalid phone number format' })
  @Field()
  phone?: string;

  @IsOptional()
  @Matches(phonePassRegEx, { message: 'Invalid phone password format' })
  @Field()
  phone_password?: string;

  @IsOptional()
  @IsNotEmpty()
  @IsBoolean()
  @Field(() => Boolean, { defaultValue: true })
  active?: boolean;

  @IsOptional()
  @IsNotEmpty()
  @IsBoolean()
  @Field(() => Boolean, { defaultValue: false })
  isAdmin?: boolean;
}
