import { ObjectType, Field } from '@nestjs/graphql';

@ObjectType()
export class AdminType {
  @Field()
  id: string;

  @Field()
  email: string;

  @Field()
  password: string;

  @Field()
  firstName: string;

  @Field()
  lastName: string;

  @Field()
  phone: string;

  @Field()
  phone_password: string;

  @Field()
  active: boolean;

  @Field()
  isAdmin: boolean;
}
