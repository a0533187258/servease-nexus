import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Admins {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ unique: true })
  email: string;

  @Column()
  password: string;

  @Column({ type: 'jsonb', nullable: true })
  firstName: string;

  @Column({ type: 'jsonb', nullable: true })
  lastName: string;

  @Column()
  phone: string;

  @Column()
  phone_password: string;

  @Column({ default: true })
  active: boolean;

  @Column({ default: false })
  isAdmin: boolean;
}
