import { Module } from '@nestjs/common';
import { AdminsService } from './admins.service';
import { AdminsResolver } from './admins.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Admins } from './entities/admin.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Admins])],
  providers: [AdminsResolver, AdminsService],
  exports: [AdminsService],
})
export class AdminsModule {}
