import { Logger, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { LoginModule } from './login/login.module';
import { ApolloServerPluginLandingPageLocalDefault as winner } from '@apollo/server/plugin/landingPage/default';
import { GraphQLModule } from '@nestjs/graphql';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { TypeOrmModule } from '@nestjs/typeorm';
import { config } from 'dotenv';
import { AdminsModule } from './admins/admins.module';
import { ConfigModule } from '@nestjs/config';

config({ path: '../.env' });

console.log(3);
@Module({
  imports: [
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      autoSchemaFile: true,
      path: '/api',
      // playground: false,
      // plugins: [winner()],
    }),
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.PG_HOST || 'localhost',
      port: Number(process.env.PG_PORT) || 5432,
      username: process.env.PG_USERNAME || 'simha',
      password: process.env.PG_PASSWORD || 'A@a66442',
      database: process.env.PG_DATABASE || 'ServEase',
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      synchronize: true,
      logging: true,
    }),
    LoginModule,
    AdminsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  // private readonly logger = new Logger(AppModule.name);
  // constructor() {
  //   this.logger.log('AppModule initialized');
  // }
}
