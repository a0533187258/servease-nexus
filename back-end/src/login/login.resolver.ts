import { Resolver, Query, Args, Int } from '@nestjs/graphql';
import { AdminsService } from 'src/admins/admins.service';
import { LoginService } from './login.service';
import { generateAuthToken } from 'src/auth/Providers/jwt';

@Resolver()
export class LoginResolver {
  // private readonly logger = new Logger(LoginResolver.name);
  constructor(
    private readonly loginService: LoginService,
  ) {}

  @Query(() => String, { name: 'login' })
  async Login(
    @Args('username') username: string,
    @Args('password') password: string,
  ): Promise<string> {
    const admin = await this.loginService.CheckingIfUserExists(
      username,
      password,
    );
    if (admin) {
      return this.loginService.generateToken(admin);
    } else {
      return 'Invalid credentials';
    }
  }
}
