import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Uservvv {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  username: string;

  @Column()
  password: string;
}
