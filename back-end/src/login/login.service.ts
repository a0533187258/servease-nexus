import { Injectable } from '@nestjs/common';
import { JwtPayload } from 'jsonwebtoken';
import { JwtService } from '@nestjs/jwt';
import { AdminsService } from 'src/admins/admins.service';
import { generateAuthToken, verifyAuthToken } from 'src/auth/Providers/jwt';
import { Admins } from 'src/admins/entities/admin.entity';

@Injectable()
export class LoginService {
  constructor(
    // private readonly jwtService: JwtService,
    private readonly adminsService: AdminsService,
  ) {}

  async CheckingIfUserExists(
    username: string,
    password: string,
  ): Promise<Admins | undefined> {
    const admin = await this.adminsService.findByUsernameAndPassword(
      username,
      password,
    );
    return admin;
  }

  generateToken(admin :Admins) {
    const token = generateAuthToken(admin);
    return token;
  };

  verifyToken(token: string) {
    try {
      const decodedUser = verifyAuthToken(token) ;
      return decodedUser;
    } catch (error) {
      console.error('Token verification failed:');
      return null;
    }
  }
}
