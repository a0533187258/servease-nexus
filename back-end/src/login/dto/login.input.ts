import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class LoginDTO {
  @Field({ description: 'username is the email' })
  username: string;

  @Field({ description: 'pasword for web' })
  password: string;
}
