## Connections are required

To run this project you need an available connection to redis and an available connection to postgres

You can connect to the cloud service of [redis](https://redis.com/cloud/overview/) and [postgres](https://www.postgresql.org/)

You can also run [Docker](https://www.docker.com/) containers in a Linux environment by the following commands:

```bash
docker run --name pg-container-name -p 5432:5432 -e POSTGRES_USER=simha -e POSTGRES_PASSWORD=A@a66442 -e POSTGRES_DB=ServEase -d postgres
```

```bash
docker run -d --name redis-stack -p 6379:6379 -p 8001:8001 redis/redis-stack:latest
```

