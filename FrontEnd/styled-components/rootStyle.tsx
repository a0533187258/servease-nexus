import styled from 'styled-components';

export const RootDiv = styled.div`
  width: 98.5vw;
  min-height: 98vh;
  display: flex;
  align-items: stretch;
  margin: 0;
  padding: 0;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-shadow: 0px 4px 8px -2px rgba(0, 0, 0, 0.4);
  background-color: #f9f9f9;
`;

export const HomeDiv = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  width: 100%;
  /* height: 100%; */
  background-color: #f9f9f9;
  padding: 20px;
  border-radius: 4px;
  box-shadow: 0px 4px 8px -2px rgba(0, 0, 0, 0.4);
  margin: 20px;
`

export const ButtonToLogin = styled.button`
    background-color: #f36edd;
  color: white;
  padding: 10px 20px;
  border: none;
  border-radius: 5px;
  cursor: pointer;
  transition: background-color 0.3s ease;

  &:hover {
    background-color: #f72bd5;
  }
`
export const LoginPageDiv = styled.div`
width: 100%;
    display: flex;
  justify-content: center;
  align-items: center;
`

export const LoginFormDiv = styled.div`
  width: 90%;
  padding: 20px;
  background-color: #f8f9fa;
  border-radius: 10px;
  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.4);
  display: flex;
flex-direction: column;
  align-items: center;
  `


export const LoginFormForm = styled.form`
display: flex;
flex-direction: column;
width: 90%;
align-items: center;
`;

export const LoginFormGroup = styled.div`
  position:relative; 
  margin: 18px 0;
  border-bottom: 1px solid #6e0082;
  transition: border-bottom-color 0.5s;
  `;

export const LoginLabel = styled.label`
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    font-size: 16px;
    color: #6e0082;
    pointer-events: none;
    transition: 0.5s;
  `;

export const LoginInput = styled.input.attrs<{ $size?: string; }>(props => ({
  // we can define static props
  type: "text",

  // or we can define dynamic ones
  $size: props.$size || "1em",
}))`
  color: #BF4F74;
  font-size: 1em;
  border: 2px solid #BF4F74;
  border-radius: 3px;

  /* here we use the dynamically computed prop */
  margin: ${props => props.$size};
  padding: ${props => props.$size};
`;


// styled.input`
//     width: 320px;
//     height: 40px;
//     font-size: 16px;
//     color: #54e28a;
//     padding: 10px;
//     background: transparent;
//     border: none;
//     outline: none;
// `;

export const LoginSubmitButton = styled.button`
padding: 10px;
border: none;
border-radius: 5px;
background-color: #f36edd;
color: white;
font-size: 16px;
cursor: pointer;
`;