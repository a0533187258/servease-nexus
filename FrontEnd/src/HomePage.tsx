import { HomeDiv, ButtonToLogin } from "../styled-components/rootStyle"
import { Link } from 'react-router-dom';

function HomePage() {
    return (
        <HomeDiv>
            <a href="https://www.yemot.co.il">
                <img width="682" height="203" src="https://www.yemot.co.il/wp-content/uploads/2022/07/לוגו-לאתר2.gif" alt="" />
            </a>
            <p>
                ברוכים הבאים
                <br />
                ServEase-Nexus
                <br />
                התחברות לאתר
            </p>
            <ButtonToLogin>
                <Link to="/login">התחברות</Link>
            </ButtonToLogin>
        </HomeDiv>
    )
}

export default HomePage