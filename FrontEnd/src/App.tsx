// import './App.css'
import { RootDiv } from "../styled-components/rootStyle";
import Router from "./router/router";

function App() {

  return (
    <RootDiv>
      <Router />
    </RootDiv>
  )
}

export default App
