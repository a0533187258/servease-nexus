import { LoginPageDiv } from "../../styled-components/rootStyle"
import LoginForm from "./LoginForm"

function LoginPage() {
    return (
        <LoginPageDiv >
            <LoginForm />
        </LoginPageDiv>
    )
}

export default LoginPage