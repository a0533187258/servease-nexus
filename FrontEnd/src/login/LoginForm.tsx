import { useForm } from 'react-hook-form';
import { LoginFormDiv, LoginFormForm, LoginFormGroup, LoginInput, LoginLabel, LoginSubmitButton } from '../../styled-components/rootStyle';

interface FormData {
    username: string;
    password: string;
}

const LoginForm: React.FC = () => {
    const { register, handleSubmit } = useForm<FormData>();

    const onSubmit = (data: FormData) => {
        // Here you can handle form submission
        console.log(data);
    };


    return (
        <LoginFormDiv>
            <h2>התחברות</h2>
            <LoginFormForm onSubmit={handleSubmit(onSubmit)}>
                <LoginFormGroup>
                    {/* <LoginLabel>שם משתמש:</LoginLabel> */}
                    <LoginInput type="text" placeholder='שם משתמש'{...register('username')} />
                </LoginFormGroup>
                <LoginFormGroup>
                    {/* <LoginLabel>סיסמא:</LoginLabel> */}
                    <LoginInput type="password" placeholder='סיסמא'{...register('password')} />
                </LoginFormGroup>
                <LoginSubmitButton type="submit">התחבר</LoginSubmitButton>
            </LoginFormForm>
        </LoginFormDiv>
    )
}

export default LoginForm