import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react';
import { config } from "dotenv";

config({ path: '../.env' });

const port =  7160

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  server: {
    host: true,
    port: port,
    strictPort: true,
    watch: {
      usePolling: true,
    },
  },
})
